from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app.model.models import db
from app import create_app

appConfg=create_app('config')

migrate = Migrate(appConfg, db)
manager = Manager(appConfg)
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
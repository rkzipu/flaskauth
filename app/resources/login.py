import random
import re
import datetime
import functools
import pymongo
import jwt
from flask import Flask, request
from flask_mail import Mail, Message
from flask_restful import Resource, Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
import requests
import config
from app.model.models import db,User
from config import *



class Login(Resource):
    def post(self):
        authType = request.json['authType']
        if authType == "account_kit":
            return Login.account_kit_login(self)

        try:
            email = request.json['email']
            password = request.json['password']
        except Exception as e:
            abort(400, message='Parameter missing '+str(e))

        user = db.session.query(User.email,User.password).filter_by(email=email).first()
        # print(user)
        if user is  None:
            abort(400, message='User is not found.')
        storedPass = user[1]
        if not check_password_hash(storedPass, password):
            abort(400, message='Password is incorrect.')
        exp = datetime.datetime.utcnow() + datetime.timedelta(hours=config.TOKEN_EXPIRE_HOURS)
        encoded = jwt.encode({'email': email, 'exp': exp},
                             config.KEY, algorithm='HS256')
        return {'email': email, 'token': encoded.decode('utf-8')}

    def account_kit_login(self):

        try:
            code = request.json['account_kit_auth_code']
        except:
            abort(400, message='Account kit code  is missing.')

        # return str(code)
        try:
            token_url = 'https://graph.accountkit.com/' + config.ACCOUNT_KIT_VERSION + '/access_token'
            token_params = {'grant_type': 'authorization_code',
                            'code': code,
                            'access_token': 'AA|%s|%s' % (config.APP_ID, config.ACCOUNT_KIT_APP_SECRET)
                            }

            r = requests.get(token_url, params=token_params)
            token_response = r.json()
            user_access_token = token_response.get('access_token')
            if user_access_token is None:
                return {'message' : token_response.get("error",{}).get("message")} , 400

            identity_url = 'https://graph.accountkit.com/' + config.ACCOUNT_KIT_VERSION + '/me'
            identity_params = {'access_token': user_access_token}
            r = requests.get(identity_url, params=identity_params)
            identity_response = r.json()

            if identity_response is None:
                return identity_response

            phone_number = identity_response.get('phone', {}).get('number', 'N/A')
            # email_address = identity_response.get('email', {}).get('address', 'N/A')
            # if email_address is not None:
            #    userDic = {"email": email_address}
            if phone_number is not "N/A":
                userDic = {"phone": phone_number}
                phoneUser = User.query.filter_by(phone=phone_number).first()
                if phoneUser is None:
                    userToInsert=User(userDic,"",True)
                    db.session.add(userToInsert)
                    db.session.commit()
                exp = datetime.datetime.utcnow() + datetime.timedelta(hours=config.TOKEN_EXPIRE_HOURS)
                encoded = jwt.encode({'phone': phone_number, 'exp': exp},
                                     config.KEY, algorithm='HS256')

                return {'phone': phone_number, 'token': encoded.decode('utf-8')}

        except Exception as e:
           return str(e)

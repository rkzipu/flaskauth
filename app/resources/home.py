from flask.json import jsonify
from flask_restful import Resource
import functools
from flask_restful import Resource, Api, abort
from flask import Flask, request
import jwt
from app.model.models import db,User,UserSchema
import config


def login_required(method):
    @functools.wraps(method)
    def wrapper(self):
        header = request.headers.get('Authorization')
        _, token = header.split()
        try:
            decoded = jwt.decode(token, config.KEY, algorithms='HS256')
        except jwt.DecodeError:
            abort(400, message='Token is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400, message='Token is expired.')
        decodedEmail = decoded['email']
        print(decodedEmail)
        user = db.session.query(User.email,User.active).filter_by(email = decodedEmail).first()

        if user is None:
            abort(400, message='User is not found.')

        return method(self, user)
    return wrapper


user_schema = UserSchema()

class Home(Resource):
    @login_required
    def get(self,user):
        userInfo = user_schema.dump(user).data
        return {'status': 'success', 'data': userInfo}, 200
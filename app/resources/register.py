import random
import re
import datetime
import functools
import pymongo
import jwt
from flask import Flask, request
from flask.json import jsonify
from flask_mail import Mail, Message
from flask_restful import Resource, Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
from app.model.models import db,User
import config

class Register(Resource):
    def post(self):
        email = request.json['email']
        password = request.json['password']

        # return str(email+password)
        if not re.match(r'^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$', email):
            abort(400, message='email is not valid.')
        if len(password) < 6:
            abort(400, message='password is too short.')

        # user = db.session.query(User).filter_by(email=email)
        # user = User.query.filter_by(email=email).all()
        user = db.session.query(User.email,User.active).filter_by(email=email).first()


        if user is not None:
            is_active = user[1]
            if is_active==True:
                abort(400, message='email is already used.')
            if is_active == False:
                abort(400, message='email is already used.not verify yet')
        else:
            userDic = {"email": email}
            user = User( userDic,generate_password_hash(password),False)
            # db.users.insert_one({'email': email, 'password': generate_password_hash(password), 'active': False})
            db.session.add(user)
            db.session.commit()

        exp = datetime.datetime.utcnow() + datetime.timedelta(days=config.ACTIVATION_EXPIRE_DAYS)
        encoded = jwt.encode({'email': email, 'exp': exp},
                             config.KEY, algorithm='HS256')
        token = encoded.decode('utf-8')
        return {"token": token}
        # try:
        #     msg = Message(recipients=[email],
        #                   body=message,
        #                   subject='Activation Code')
        #     mail.send(msg)
        # except Exception as e:
        #
        #     return str(e+" token:  "+encoded.decode('utf-8'))
        # return {'message': "Activation Code sent to your email."}
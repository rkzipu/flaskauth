import random
import re
import datetime
import functools
import pymongo
import jwt
from flask import Flask, request
from flask_mail import Mail, Message
from flask_restful import Resource, Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
import config
from app.model.models import db,User




class Activate(Resource):
    def put(self):
        activation_code = request.json['activation_code']
        try:
            decoded = jwt.decode(activation_code, config.KEY, algorithms='HS256')
        except jwt.DecodeError:
            abort(400, message='Activation code is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400, message='Activation code is expired.')
        tokenEmail = decoded['email']

        try:
            db.session.query(User). \
                filter_by(email = tokenEmail). \
                update({"active": True})
            db.session.commit()
        except Exception as e:
            print(e)
            abort(400, message='User activation failed')

        return {'email': tokenEmail,'status':'activated'}

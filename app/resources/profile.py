from flask.json import jsonify
from flask_restful import Resource
import functools
from flask_restful import Resource, Api, abort
from flask import Flask, request
import jwt
from app.model.models import db,User,UserSchema
import config


def login_required(method):
    @functools.wraps(method)
    def wrapper(self):
        header = request.headers.get('Authorization')
        _, token = header.split()
        try:
            decoded = jwt.decode(token, config.KEY, algorithms='HS256')
        except jwt.DecodeError:
            abort(400, message='Token is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400, message='Token is expired.')
        decodedEmail = decoded['email']
        print(decodedEmail)
        user =  User.query.filter_by(email= decodedEmail).first()

        if user is None:
            abort(400, message='User is not found.')

        return method(self, user)
    return wrapper


user_schema = UserSchema()

class Profile(Resource):
    @login_required
    def get(self,user):
        userInfo = user_schema.dump(user).data
        return {'status': 'success', 'data': userInfo}, 200

    @login_required
    def put(self,user):
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = user_schema.load(json_data)
        if errors:
            return errors, 422
        tempUser = User.query.filter_by(email=data['email']).first()
        if not tempUser:
            return {'message': 'Category does not exist'}, 400
        if "name" in data.keys():
            tempUser.name = data['name']
        if "role" in data.keys():
            tempUser.role = data['role']
        if "phone" in data.keys():
            tempUser.mobile = data['phone']
        if "img_url" in data.keys():
            tempUser.img_url = data['img_url']
        isUpdate = db.session.commit()
        print(isUpdate)
        result = user_schema.dump(tempUser).data
        print(result)
        return {'status': 'success', 'data': result}, 202
# coding: utf-8
from flask import Flask
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Boolean
from sqlalchemy.schema import FetchedValue
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields, pre_load, validate
# app = Flask(__name__)
# app.config.from_object('config')


ma = Marshmallow()
db = SQLAlchemy()


# Define a User model
class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    # User Name
    name    = db.Column(db.String(128),  nullable=True)

    # Identification Data: email & password
    email    = db.Column(db.String(128),  nullable=True,
                                            unique=True)
    password = db.Column(db.String(192),  nullable=True)

    # Authorisation Data: role & status
    role     = db.Column(db.String(20), nullable=True)
    img_url     = db.Column(db.String(200), nullable=True)
    role     = db.Column(db.String(20), nullable=True)
    phone   = db.Column(db.String(15), nullable=True,unique=True)
    active   = db.Column(Boolean, unique=False)


    # New instance instantiation procedure
    def __init__(self, user , password , active):
        self.active     = active
        if 'email' in user.keys():
            self.email = user["email"]
        if 'phone' in user.keys():
            self.phone = user["phone"]
        self.password = password


    def __repr__(self):
        return '<User %r>' % (self.name)

class UserSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    email = fields.String(required=True)
    phone = fields.String(required=True)
    name = fields.String(required=False)
    role = fields.String(required=False)
    active = fields.Boolean(required=False)
    img_url = fields.String(required=False)

from flask_restful import Resource,Api
# third-party imports
from flask import Flask
from flask import Blueprint

api_bp=Blueprint('api',__name__)
api = Api(api_bp)

from app.resources.home import Home
from app.resources.profile import Profile
from app.resources.login import Login
from app.resources.register import Register
from app.resources.activate import Activate



def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_name)

    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from app.model.models import db
    db.init_app(app)

    return app


api.add_resource(Register, '/v1/register')
api.add_resource(Activate, '/v1/activate')
api.add_resource(Login, '/v1/login')
api.add_resource(Home, '/v1/home')
api.add_resource(Profile, '/v1/user/profile')


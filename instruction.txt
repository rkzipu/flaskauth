#to start xampp
sudo /opt/lampp/lampp start

#flask db migrate
python migrate.py db init
python migrate.py db migrate
python migrate.py db upgrade

#auto generate model
flask-sqlacodegen --flask --outfile models.py mysql+pymysql://root@localhost/flask_api_db
flask-sqlacodegen --flask --outfile models.py --tables 'users'  mysql+pymysql://root@localhost/flask_api_db
import os

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI= 'mysql+pymysql://root@localhost/flask_api_db'

DEBUG = True
MAIL_SERVER = 'smtp.googlemail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'your@gmail.com'
MAIL_PASSWORD = 'pass'
MAIL_DEFAULT_SENDER = 'your@gmail.com'
MONGO_DB_HOST = '127.0.0.1'
MONGO_DB_PORT = 27017
KEY = 'secret'
ACTIVATION_EXPIRE_DAYS = 5
TOKEN_EXPIRE_HOURS = 1
SECRET_KEY = 'My own secret key'

APP_ID = "1438927536129584"
ACCOUNT_KIT_APP_SECRET = "0edbbcb75cf48231855adb37d0653bcf"
ACCOUNT_KIT_VERSION = 'v1.1'



